#!/usr/bin/env bash

# A script to automatically keep Raspberry Pi's LEDs
# off at night
#
# Author:   Radek SPRTA
# Date:     2016/10/10
# License:  MIT License

set -o errexit          # Exit on most errors (see the manual)
set -o errtrace         # Make sure any error trap is inherited
set -o nounset          # Disallow expansion of unset variables
set -o pipefail         # Use last non-zero exit code in a pipeline

export PATH=/bin:/usr/bin:"${PATH}"

NIGHT=23   # Hour to turn LEDs off
DAY=9      # Hour to turn LEDs on
VERBOSE=false

show_usage() {
  printf "
Usage: $0 [OPTIONS]

Options:

    -h          show this help and exit
    -d NUMBER   hour to turn on LEDs
    -n NUMBER   hour to turn off LEDs
    -v          output
" $(basename $0) >&2
  exit 1
}

turn_leds_on() {
  echo mmc0 | sudo tee /sys/class/leds/led0/trigger > /dev/null
  echo 255 | sudo tee /sys/class/leds/led1/brightness > /dev/null
}

turn_leds_off() {
  echo none | sudo tee /sys/class/leds/led0/trigger > /dev/null
  echo 0 | sudo tee /sys/class/leds/led1/brightness > /dev/null
}

while getopts 'hd:n:v' option; do
  case ${option} in
    h) show_usage ;;
    d) DAY=${OPTARG} ;;
    n) NIGHT=${OPTARG} ;;
    v) VERBOSE=true ;;
    *) show_usage ;;
  esac
done

current_time=$(date +%H)
if [ "${current_time}" -lt "${NIGHT}" -a "${current_time}" -ge "${DAY}" ]; then 
  turn_leds_on
  if [ "${VERBOSE}" == true ]; then
    printf "LEDs turned on"
  fi
else
  turn_leds_off
  if [ "${VERBOSE}" == true ]; then
    printf "LEDs turned off"
  fi
fi