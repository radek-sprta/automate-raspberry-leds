A simple bash script and an accompanying systemd service file to keep Raspberry 
PI's LEDs turned off at night.

## Install 

* Copy `raspberry-leds.sh` to `/usr/local/bin`
* Make it executable with `sudo chmod +x /usr/local/bin/raspberry-leds.sh`
* Copy `raspberry-leds.service` and `raspberry-leds.timer` to `/etc/systemd/system/multi-user.target.wants/`
* Run `sudo systemctl daemon-reload`